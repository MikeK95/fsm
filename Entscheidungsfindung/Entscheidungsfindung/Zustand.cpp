#include "pch.h"
#include "Zustand.h"


Zustand::Zustand(std::string Name)
{
	m_Name = Name;
}


Zustand::~Zustand()
{
}

void Zustand::addAktion(std::string Aktion)
{
	m_Aktion = Aktion;
}

//void Zustand::addAusgangsAktion(std::string AusgangsAktion)
//{
//	m_AusgangsAktionen.push_back(AusgangsAktion);
//}
//
//void Zustand::addEingangsAktion(std::string EingangsAktion)
//{
//	m_EingangsAktionen.push_back(EingangsAktion);
//}

void Zustand::AddUebergang(Uebergang *aUebergang)
{
	m_Uebergang.push_back(aUebergang);
}

std::string Zustand::getAktion()
{
	return m_Aktion;
}


//std::vector<std::string> Zustand::getAusgangsAktion()
//{
//	return m_AusgangsAktionen;
//}
//
//std::vector<std::string> Zustand::getEingangsAktion()
//{
//	return m_EingangsAktionen;
//}

std::string Zustand::getName()
{
	return m_Name;
}

std::vector<Uebergang*> Zustand::getUebergang()
{
	return m_Uebergang;
}

