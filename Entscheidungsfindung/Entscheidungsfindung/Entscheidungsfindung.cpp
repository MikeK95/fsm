// Entscheidungsfindung.cpp : Diese Datei enthält die Funktion "main". Hier beginnt und endet die Ausführung des Programms.
//

#include "pch.h"
#include <iostream>
#include "FSM.h"
#include <Windows.h>
#include <ctime>

int main()
{
	std::srand(std::time(nullptr));
    std::cout << "Hello World!\n"; 

	FSM Wache("Wache");
	Wache.addZustand("Patrouillieren");
	Wache.addZustand("Essen");
	Wache.addZustand("Verteidigen");

	Wache.addAktionToZustand("Patrouillieren", "am patrouillieren");
	Wache.addAktionToZustand("Essen", "am essen");
	Wache.addAktionToZustand("Verteidigen", "am ballern");

	Wache.addUebergang("Hunger", "Patrouillieren", "Essen");
	Wache.addUebergang("Satt", "Essen", "Patrouillieren");
	Wache.addUebergang("FeindGreiftAn", "Patrouillieren", "Verteidigen");
	Wache.addUebergang("GefahrVorbei", "Verteidigen", "Essen");

	Wache.addAktionToUebergang("Hunger", "OHNE MAMPF KEIN KAMPF");
	Wache.addAktionToUebergang("Satt", "ARBEIT RUFT");
	Wache.addAktionToUebergang("FeindGreiftAn", "ANGRIFF!!!");
	Wache.addAktionToUebergang("GefahrVorbei", "KAEMPFEN MACHT HUNGRIG");

	Wache.addBedingung("Hunger", 0);
	Wache.addBedingung("Satt", 10);
	Wache.addBedingung("FeindGreiftAn", 11);
	Wache.addBedingung("GefahrVorbei", 0);

	Wache.setStartZustand("Patrouillieren");

	do
	{
		Wache.update();
		Sleep(1000);
	}while (true);
}

// Programm ausführen: STRG+F5 oder "Debuggen" > Menü "Ohne Debuggen starten"
// Programm debuggen: F5 oder "Debuggen" > Menü "Debuggen starten"

// Tipps für den Einstieg: 
//   1. Verwenden Sie das Projektmappen-Explorer-Fenster zum Hinzufügen/Verwalten von Dateien.
//   2. Verwenden Sie das Team Explorer-Fenster zum Herstellen einer Verbindung mit der Quellcodeverwaltung.
//   3. Verwenden Sie das Ausgabefenster, um die Buildausgabe und andere Nachrichten anzuzeigen.
//   4. Verwenden Sie das Fenster "Fehlerliste", um Fehler anzuzeigen.
//   5. Wechseln Sie zu "Projekt" > "Neues Element hinzufügen", um neue Codedateien zu erstellen, bzw. zu "Projekt" > "Vorhandenes Element hinzufügen", um dem Projekt vorhandene Codedateien hinzuzufügen.
//   6. Um dieses Projekt später erneut zu öffnen, wechseln Sie zu "Datei" > "Öffnen" > "Projekt", und wählen Sie die SLN-Datei aus.
