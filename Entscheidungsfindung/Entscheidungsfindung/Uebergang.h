#pragma once
#include <vector>
#include "Zustand.h"

class Zustand;

class Uebergang
{
public:
	Uebergang(std::string name, Zustand* zielzustand);
	~Uebergang();
	void AddAktion(std::string Aktion);
	std::string getAktion();
	Zustand* GetZielzustand();
	bool isTriggered(int Bedingung);
	void SetBedingung(int Bedingung);
	std::string getName();


private:
	std::string m_Aktion;
	int m_bedingung;
	Zustand* m_Zielzustand;
	std::string m_Name;

};

