#pragma once
#include "Zustand.h"
#include "Uebergang.h"
#include <map>
#include <iostream>

class FSM
{
public:
	FSM(std::string Name);
	~FSM();

	void addAktionToZustand(std::string Zustand, std::string Aktion);
	void addAktionToUebergang(std::string Uebergang, std::string Aktion);
	void addBedingung(std::string Uebergang, int Bedingung);
	void addUebergang(std::string NameUebergang, std::string fromZustand, std::string toZustand);
	void addZustand(std::string Name);
	Zustand* getZustand(std::string Name);
	void printStatus();
	void setStartZustand(std::string Name);
	void update();

	void checkFood();

private:
	std::string m_name;
	Zustand* m_aktuellerZustand;
	//Option* m_currentbestoption;
	Zustand* m_initialerZustand;
	//bool m_performTransition;
	std::map<std::string, Zustand*> m_zustaende;
	int food = 9;
};

