#pragma once
#include <vector>
#include "Uebergang.h"
#include <string>

class Uebergang;

class Zustand
{
public:
	Zustand(std::string Name);
	~Zustand();
	void addAktion(std::string Aktion);
	//void addAusgangsAktion(std::string AusgangsAktion);
	//void addEingangsAktion(std::string EingangsAktion);
	void AddUebergang(Uebergang *aUebergang);
	std::string getAktion();
	//std::vector<std::string> getAusgangsAktion();
	//std::vector<std::string> getEingangsAktion();
	std::string getName();
	std::vector<Uebergang*> getUebergang();


private:
	std::string m_Aktion;
	//std::vector<std::string> m_AusgangsAktionen;
	//std::vector<std::string> m_EingangsAktionen;
	std::string m_Name;
	std::vector<Uebergang*> m_Uebergang;

};

