#include "pch.h"
#include "FSM.h"


FSM::FSM(std::string Name)
{
	m_name = Name;
}


FSM::~FSM()
{
}

void FSM::addAktionToZustand(std::string Zustand, std::string Aktion)
{
	m_zustaende.find(Zustand)->second->addAktion(Aktion);
}

void FSM::addAktionToUebergang(std::string Uebergang, std::string Aktion)
{
	for (const auto& iter : m_zustaende)
	{
		for (const auto& iter2 : iter.second->getUebergang())
		{
			if (iter2->getName() == Uebergang)
				iter2->AddAktion(Aktion);
		}
	}
}

void FSM::addBedingung(std::string Uebergang, int Bedingung)
{
	for (const auto& iter : m_zustaende)
	{
		for (const auto& iter2 : iter.second->getUebergang())
		{
			if (iter2->getName() == Uebergang)
				iter2->SetBedingung(Bedingung);
		}
	}
}

void FSM::addUebergang(std::string NameUebergang, std::string fromZustand, std::string toZustand)
{
	Uebergang* newUebergang = new Uebergang(NameUebergang, m_zustaende.find(toZustand)->second);
	m_zustaende.find(fromZustand)->second->AddUebergang(newUebergang);
}

void FSM::addZustand(std::string Name)
{
	Zustand* newZustand = new Zustand(Name);
	m_zustaende.insert(std::pair<std::string, Zustand*>(Name, newZustand));
}

Zustand * FSM::getZustand(std::string Name)
{
	return m_zustaende.find(Name)->second;
}

void FSM::printStatus()
{
	std::cout << m_aktuellerZustand->getAktion();
}

void FSM::setStartZustand(std::string Name)
{
	m_initialerZustand = m_zustaende.find(Name)->second;
	m_aktuellerZustand = m_initialerZustand;
}

void FSM::update()
{
	int temp = food;
	int v1 = std::rand() % 5;
	if (v1 == 2 && m_aktuellerZustand->getName() != "Verteidigen")
		food = 11;

	for (const auto& iter2 : m_aktuellerZustand->getUebergang())
	{
		if (iter2->isTriggered(food))
		{
			std::cout<<iter2->getAktion() << std::endl;
			m_aktuellerZustand = iter2->GetZielzustand();
		}
	}
	food = temp;
	checkFood();
	printStatus();
	std::cout << "(Futter: " << food << ")" << std::endl;

}

void FSM::checkFood()
{
	if (m_aktuellerZustand->getName() == "Patrouillieren" || m_aktuellerZustand->getName() == "Verteidigen")
		food--;
	else
		food+=2;
}
