#include "pch.h"
#include "Uebergang.h"

Uebergang::Uebergang(std::string name, Zustand* zielzustand)
{
	m_Name = name;
	m_Zielzustand = zielzustand;
}


Uebergang::~Uebergang()
{
}

void Uebergang::AddAktion(std::string Aktion)
{
	m_Aktion = Aktion;
}

std::string Uebergang::getAktion()
{
	return m_Aktion;
}

Zustand * Uebergang::GetZielzustand()
{
	return m_Zielzustand;
}

bool Uebergang::isTriggered(int Bedingung)
{	
	if (m_bedingung == Bedingung)
		return true;
	else
		return false;
}

void Uebergang::SetBedingung(int Bedingung)
{
	m_bedingung = Bedingung;
}

std::string Uebergang::getName()
{
	return m_Name;
}
